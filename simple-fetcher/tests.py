import unittest
import tweepy
import os
import sys
import sqlite3
from sqlite3 import Error
import unittest
import fetch

class TestFetch(unittest.TestCase):
    
    def tearDown(self):
        if os.path.exists("pje-c_tweet.db"):
            os.remove("pje-c_tweet.db")
    
    def test_create_db_file_exists(self):
        open("pje-c_tweet.db",'x').close()
        self.assertEqual(fetch.create_db_file(),0)
    
    def test_create_db_file_not_exists(self):
        self.assertEqual(fetch.create_db_file(),1)
    
    def test_nettoyage_base(self):
        chaine_basique="hello test"
        self.assertEqual(chaine_basique,fetch.nettoyage(chaine_basique))
        
    def test_nettoyage_arobase(self):
        chaine_aro="salut @franceinter comment va"
        nett_aro="salut @ comment va"
        self.assertEqual(nett_aro,fetch.nettoyage(chaine_aro))
        
    def test_nettoyage_htag(self):
        chaine_htag="le covid est un complot #decideur"
        nett_htag="le covid est un complot {htag}"
        self.assertEqual(nett_htag,fetch.nettoyage(chaine_htag))
        
    def test_nettoyage_link(self):
        chaine_link="allez voir https://monsite.fr"
        nett_link="allez voir {link}"
        self.assertEqual(nett_link,fetch.nettoyage(chaine_link))
    
    def test_nettoyage_ponctuation(self):
        chaine_ponc="ça va!et toi?J'ai pas le temps,bref;à plus."
        nett_ponc="ça va ! et toi ? J'ai pas le temps , bref ; à plus . "
        self.assertEqual(nett_ponc,fetch.nettoyage(chaine_ponc))
        
    def test_nettoyage_lien_twitter(self):
        chaine="https://t.co/eijfieofgoejgnosing44zf446zf"
        nett="{tCo}"
        self.assertEqual(nett,fetch.nettoyage(chaine))
        
    def test_nettoyage_monnaie(self):
        chaine="J'ai $20 là ou 20€ comme tu veux ouais c'est pas pareil tg"
        nett="J'ai $XXX là ou XXX€ comme tu veux ouais c'est pas pareil tg"
        self.assertEqual(nett,fetch.nettoyage(chaine))
        
    def test_nettoyage_percent(self):
        chaine="Je suis à 85% et toi chenapan à 100000%"
        nett="Je suis à XX% et toi chenapan à XX%"
        self.assertEqual(nett,fetch.nettoyage(chaine))

    def test_connection_db(self):
        conn=fetch.connection_db("")
        self.assertTrue(fetch.check_conn(conn))
            
    def test_connection_twitter(self):
        api=fetch.connection_twitter()
        self.assertEqual(api.verify_credentials().name,"hf_vm_dev | MASTER")
        
    def test_connection_twitter_fail(self):
        key,secret,token,access=" "," "," "," "
        auth=tweepy.OAuth1UserHandler(key,secret)
        auth.set_access_token(token,access)
        api=tweepy.API(auth)
        with self.assertRaises(tweepy.errors.Unauthorized):
            print(api.verify_credentials())
        
    def test_insert(self):
        fetch.create_db_file()
        path=""
        conn=fetch.connection_db(path)
        api=fetch.connection_twitter()
        theme=fetch.get_theme("../")
        tweets=fetch.fetch_tweet(api, 1, theme)
        t=fetch.insert_tweet(conn,api,tweets)
        cur=conn.cursor()
        sql_sel=''' SELECT * from tweets ORDER BY id DESC LIMIT 1 '''
        res=cur.execute(sql_sel)
        for line in res:
            self.assertEqual(line[1],t[0])
            self.assertEqual(line[3],t[1])
            self.assertEqual(line[2],t[2])
        sql_del=''' DELETE FROM tweets WHERE id_tweet=?'''
        cur.execute(sql_del,(t[0],))
        conn.commit()
        res=cur.execute(sql_sel)
        for line in res:
            self.assertNotEqual(line[1],t[0])
            self.assertNotEqual(line[2],t[1])
            self.assertNotEqual(line[3],t[2])
            
          
        
if __name__ == '__main__':
    unittest.main()