import tweepy
import re
import os
import sys
import sqlite3
import random

def check_conn(conn):
    try:
        conn.cursor()
        return True
    except Exception as e:
        print(e)
        return False
    
def insert(conn, values):
    sql=''' INSERT OR IGNORE INTO tweets (id_tweet, content, author, date, polarite)
                VALUES (?, ?, ?, ?, -1)
        '''
    cur=conn.cursor()
    cur.execute(sql, values)
    conn.commit()
    return cur.lastrowid

def create_db_file():
     if os.path.exists("pje-c_tweet.db"):
         return 0
     open("pje-c_tweet.db",'x').close()
     return 1

def connection_db(path):
    conn=None
    database=path+"pje-c_tweet.db"
    sql_create=""" CREATE TABLE IF NOT EXISTS tweets (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    id_tweet integer NOT NULL,
                    author text NOT NULL,
                    content text NOT NULL,
                    date text NOT NULL,
                    polarite integer NOT NULL,
                    UNIQUE(id_tweet)
                    ); """
    conn=sqlite3.connect(database)
    cur=conn.cursor()
    cur.execute(sql_create)
    print(sqlite3.version)
    return conn

def connection_twitter():
    api_key = "YdBh4A6Fk4XWTeJWtZtn2iCWr"
    api_secret = "mhfhKoIyG8KnZMQeHWuPApneXkJK35fkLmjIS0UHaLFBg8V9Le"

    access_token = "1569293081496649729-TU0s2VC9wvueZdCyupamW2RGOb0SiM"
    access_secret = "b6PVacq8k4gMcOrC0vB87Q72HQSCgMuM8qWrIcyHwqRCa"

    auth = tweepy.OAuth1UserHandler(api_key,api_secret)
    auth.set_access_token(access_token,access_secret)
 
    api = tweepy.API(auth)

    try:
        api.verify_credentials()
        print('Successful Authentication\n')
    except:
        print('Failed Authentication')
    return api

def fetch_tweet(api, nb, theme):
    theme+=" -filter:retweets"
    print(theme)
    return tweepy.Cursor(api.search_tweets,q=theme,lang='fr',result_type="mixed",count=1, tweet_mode='extended').items(nb)

def insert_tweet(conn, api, tweets):
    t=""
    for tweet in tweets:
        t=(tweet.id,nettoyage(tweet.full_text),tweet.user.screen_name,tweet.created_at)
        insert(conn,t)
    return t

def nettoyage(text):
    text=re.sub("@[a-z_A-Z0-9]*","@",text)
    text=re.sub("#[a-z_A-Z0-9]*","{htag}",text)
    text=re.sub("https://t.co/[a-zA-Z0-9]*","{tCo}",text)
    text=re.sub("https://[a-zA-Z0-9/.?]*","{link}",text)
    text=re.sub("http://[a-zA-Z0-9/.?]*","{link}",text)
    text=re.sub("[!?\"“.;…·,]", lambda val : f" {val[0]} ",text)
    text=re.sub(r'\u0024[0-9]+',"\u0024XXX",text)
    text=re.sub("[0-9]*€","XXX€",text)
    text=re.sub("[0-9]+%","XX%", text)
    return text

def get_theme(path):
    f=open(path+"themes.txt","r")
    res=random.choice(list(f))
    f.close()
    return str(res[0:-1])

def main():
    create_db_file()
    path=""
    if len(sys.argv)>1:
        path=sys.argv[1]
    conn=connection_db(path)
    api=connection_twitter()
    theme=get_theme(path)
    nb_tweets=100
    tweets=fetch_tweet(api, nb_tweets, theme)
    insert_tweet(conn,api,tweets)
if __name__ == '__main__':
    main()



