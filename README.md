# PJE - C

### Members

- Hugo Forraz @ hugo@forraz.com
- Matthieu Vannin @ matthieu.vannin.etu@univ-lille.fr

### Twitter account :

@hfvm_dev_MASTER

https://twitter.com/hfvm_dev_MASTER

### Stack technique

Récupération des tweets / Indexage :
- RaspberryPI
- Database (Psql ?)
- Python 

TUI :
- tui-rs
- Rust
