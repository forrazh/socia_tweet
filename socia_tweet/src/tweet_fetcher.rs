use twitter_v2::authorization::BearerToken;
use twitter_v2::id::IntoNumericId;
use twitter_v2::meta::TweetsMeta;
use twitter_v2::query::TweetField;
use twitter_v2::{ApiResult, Error, Tweet, TwitterApi};

/// fetch multiple tweets. If number is given, it should always be between 10 and 100.
/// Otherwise it will be 20.
pub async fn fetch(auth: BearerToken, keyword: &str, number: Option<usize>) -> Option<Vec<Tweet>> {
    let number = number.unwrap_or(20);
    let req = request_data_from_twitter(auth, keyword, number).await.ok()?;
    let data = req.into_data()?;
    Some(data)
}

async fn retrieve_single_tweet(
    auth: BearerToken,
    id: impl IntoNumericId,
) -> ApiResult<BearerToken, Tweet, ()> {
    TwitterApi::new(auth)
        .get_tweet(id)
        .tweet_fields([TweetField::Text, TweetField::AuthorId, TweetField::Id])
        .send()
        .await
}

#[tokio::test]
async fn test_retrieve_single_tweet_by_id() {
    let tweet_id = 1261326399320715264;
    let auth = BearerToken::new("AAAAAAAAAAAAAAAAAAAAADf9gwEAAAAAtUMkjIu70A2Gg%2FJGkeZN87kVRRM%3DNtcCMhavc9k9EzD8USFXFCVjqYbqS6bZdLA6gS8GQXvfLsaNWe");
    let result = retrieve_single_tweet(auth, tweet_id).await;
    assert!(result.is_ok());
    let result = result.unwrap().into_data();
    assert!(result.is_some());
    let result = result.unwrap();
    assert_eq!(result.id, tweet_id);
    assert_eq!(result.author_id.unwrap(), 2244994945);
}

async fn request_data_from_twitter(
    auth: BearerToken,
    keyword: &str,
    number: usize,
) -> ApiResult<BearerToken, Vec<Tweet>, TweetsMeta> {
    TwitterApi::new(auth)
        .get_tweets_search_recent( format!("{keyword} -is:retweet lang:FR"))
        .max_results(number)
        .tweet_fields([TweetField::Text, TweetField::AuthorId, TweetField::Id])
        .send()
        .await
}

#[tokio::test]
async fn test_retrieve_multiple_tweets() {
    let query = "foot".to_lowercase();
    let auth = BearerToken::new("AAAAAAAAAAAAAAAAAAAAADf9gwEAAAAAtUMkjIu70A2Gg%2FJGkeZN87kVRRM%3DNtcCMhavc9k9EzD8USFXFCVjqYbqS6bZdLA6gS8GQXvfLsaNWe");
    let result = request_data_from_twitter(auth, &query, 20).await;
    assert!(result.is_ok());
    let result = result.unwrap().into_data();
    assert!(result.is_some());
    let result = result.unwrap();
    assert!(!result.is_empty());
    for tweet in result {
        println!("{} {} {}", tweet.id, tweet.author_id.unwrap(), tweet.text);
        assert!(tweet.text.to_lowercase().contains(&query));
    }
}
