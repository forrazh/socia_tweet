use algorithms::alg_data::EstimatedClass;

/// TODO:
/// Add default impl
/// Add new impl
/// Some sort of setter ? (not sure about that)
/// Display the estimations using emojis like :
///
/// 👎 ; 👍 ; ❤️ ; 😊 ; 😟 ; 😥 ; 😐 ; 🗙 ; ✔️
/// Need of the following values :
/// None, Some(Neutral), Some(Positive), Some(Negative)

#[derive(Clone, Debug)]
pub struct TweetDataDisplay {
    pub name:String,
    pub content:String,
    pub tweet_id: String,
    pub keyword_estimation:Option<EstimatedClass>,
    pub knn_estimation:Option<EstimatedClass>,
    pub unigram_estimation:Option<EstimatedClass>,
    pub bigram_estimation:Option<EstimatedClass>,
    pub bi_unigram_estimation:Option<EstimatedClass>,
}

impl Default for TweetDataDisplay {
    fn default() -> Self {
        Self {
            name: String::from(""),
            content: String::from(""),
            tweet_id: String::from(""),
            keyword_estimation:None,
            knn_estimation:None,
            unigram_estimation:None,
            bigram_estimation:None,
            bi_unigram_estimation:None,
        }
    }
}

impl TweetDataDisplay {
    pub fn new(name: String, content: String, tweet_id: String) -> Self {
        Self {
            name,
            content,
            tweet_id,
            ..TweetDataDisplay::default()
        }
    }
    pub fn get_keyword_estimation(&self) -> Option<EstimatedClass> {
        self.keyword_estimation
    }
    pub fn get_knn_estimation(&self) -> Option<EstimatedClass> {
        self.knn_estimation
    }
    pub fn get_unigram_estimation(&self) -> Option<EstimatedClass> {
        self.unigram_estimation
    }
    pub fn get_bigram_estimation(&self) -> Option<EstimatedClass> {
        self.bigram_estimation
    }
    pub fn get_bi_unigram_estimation(&self) -> Option<EstimatedClass> {
        self.bi_unigram_estimation
    }
}

pub fn display_estimation(estimation: Option<EstimatedClass>) -> String {
    match estimation {
        Some(e) => match e {
            EstimatedClass::Positive => "👍".to_string(),
            EstimatedClass::Negative => "👎".to_string(),
            EstimatedClass::Neutral => "😐".to_string(),
        },
        None => "🗙".to_string()
    }
}