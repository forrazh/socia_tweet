use std::sync::Arc;
use std::sync::mpsc::Sender;
use tokio::sync::Mutex;

use tui::{
    backend::Backend,
    Frame,
    layout::{Constraint, Direction, Layout, Rect},
    style::{Color, Style},
    text::Spans,
    widgets::{
        Block, Borders, Clear/*, Gauge, List, ListItem, ListState*/, Paragraph, /* Row, Table, Wrap,*/
    },
};
use tui::widgets::{Cell, Row, Table};

use crate::data::TweetDataDisplay;
use crate::TasksEvent;

pub enum InputMode {
    Normal,
    Editing,
}

pub struct App {
    /// Current value of the input box
    pub input: String,
    /// Current input mode
    pub input_mode: InputMode,
    /// Tweets fetched
    pub tweets: Vec<TweetDataDisplay>,
    /// Display the popup
    pub show_popup: bool,
    event_sender: Option<Sender<TasksEvent>>,
}

impl App {
    pub fn new(event_sender: Sender<TasksEvent>) -> App {
        App { event_sender: Some(event_sender), ..App::default() }
    }

    pub fn dispatch(&mut self, event:TasksEvent) {
        self.show_popup = true;
        if let Some(sender) = &self.event_sender {
            if let Err(e) = sender.send(event) {
                self.show_popup = false;
                println!("Oooh, error on dispatch !: {}", e);
            }
        }
    }
}

impl Default for App {
    fn default() -> App {
        App {
            input: String::new(),
            input_mode: InputMode::Normal,
            tweets: Vec::new(),
            show_popup: false,
            event_sender: None,
        }
    }
}

fn draw_input<B>(f: &mut Frame<B>, app: &App, layout_chunk: Rect)
    where
        B: Backend,
{
    // Check for the width and change the contraints accordingly
    let chunks = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([Constraint::Percentage(100)])
        .split(layout_chunk);

    let input = Paragraph::new(app.input.as_ref())
        .style(match app.input_mode {
            InputMode::Normal => Style::default(),
            InputMode::Editing => Style::default().fg(Color::Yellow),
        })
        .block(Block::default().borders(Borders::ALL).title(" Search "));

    f.render_widget(input, chunks[0]);
}

pub fn draw_main_layout<B>(f: &mut Frame<B>, app: &App)
    where
        B: Backend,
{
    let size = f.size();

    let parent_layout = Layout::default()
        .direction(Direction::Vertical)
        .constraints(
            [
                Constraint::Percentage(10),
                Constraint::Percentage(77),
                Constraint::Percentage(13),
            ]
                .as_ref(),
        )
        .margin(1)
        .split(size);

    draw_input(f, app, parent_layout[0]);
    draw_basic_view(f, app, parent_layout[1]);
    draw_commands(f, parent_layout[2]);

    if app.show_popup {
        loading_screen(f, &app, size);
    }
}

fn draw_basic_view<B>(f: &mut Frame<B>, app:&App, area: Rect)
    where
        B: Backend,
{
    let header_cells = ["Content", "Keyword", "KNN", "Bayes-UNI", "Bayes-BI", "Bayes-BI+UNI"]
        .iter()
        .map(|h| Cell::from(*h).style(Style::default().fg(Color::Red)));
    let header = Row::new(header_cells)
        // .style(normal_style)
        .height(1)
        .bottom_margin(1);

    let rows = app.tweets.iter().map(|item:&TweetDataDisplay| {
        let cells = vec![
            Cell::from(String::from(&item.content)),
            Cell::from(crate::data::display_estimation(item.keyword_estimation)),
            Cell::from(crate::data::display_estimation(item.knn_estimation)),
            Cell::from(crate::data::display_estimation(item.unigram_estimation)),
            Cell::from(crate::data::display_estimation(item.bigram_estimation)),
            Cell::from(crate::data::display_estimation(item.bi_unigram_estimation)),
        ];
        Row::new(cells)/*.height(height as u16)*/.bottom_margin(1)
    });
    let t = Table::new(rows)
        .header(header)
        .block(Block::default().borders(Borders::ALL).title("Table"))
        // .highlight_style(selected_style)
        // .highlight_symbol(">> ")
        .widths(&[
            Constraint::Percentage(50),
            Constraint::Min(10),
            Constraint::Min(10),
            Constraint::Min(10),
            Constraint::Min(10),
            Constraint::Min(10),
        ]);
    f.render_widget(t, area);
}

fn draw_commands<B>(f: &mut Frame<B>, area: Rect)
    where
        B: Backend,
{
    let block = Block::default().title(" Commands ").borders(Borders::ALL);
    f.render_widget(block, area);

    let chunks = Layout::default()
        .direction(Direction::Horizontal)
        .margin(1)
        .constraints([Constraint::Percentage(40), Constraint::Percentage(60)])
        .split(area);

    let commands = vec![
        Spans::from("s: Search a tweet"),
        Spans::from("d: Use a Keyword classifier"),
        Spans::from("k: Use a KNN classifier"),
        Spans::from("q: Quit the application"),
    ];
    let paragraph = Paragraph::new(commands);
    f.render_widget(paragraph, chunks[0]);

    let commands = vec![
        Spans::from("v: Use a Unigram-based Bayes Classifier"),
        Spans::from("b: Use a Bigram-based Bayes Classifier"),
        Spans::from("m: Use a Unigram+Bigram-based Bayes Classifier"),
    ];
    let paragraph = Paragraph::new(commands);
    f.render_widget(paragraph, chunks[1]);
}

pub fn loading_screen<B>(f: &mut Frame<B>, app: &App, size: Rect) where B: Backend {
    let block = Paragraph::new("Loading....")
        .block(Block::default()
            .title(" Loading ")
            .borders(Borders::ALL));
    let area = centered_rect(60, 20, size);

    f.render_widget(Clear, area); //this clears out the background
    f.render_widget(block, area);
}

fn center(direction: Direction, percent: u16, r: Rect) -> Rect {
    Layout::default()
        .direction(direction)
        .constraints(
            [
                Constraint::Percentage((100 - percent) / 2),
                Constraint::Percentage(percent),
                Constraint::Percentage((100 - percent) / 2),
            ]
                .as_ref(),
        )
        .split(r)[1]
}

/// helper function to create a centered rect using up certain percentage of the available rect `r`
fn centered_rect(percent_x: u16, percent_y: u16, r: Rect) -> Rect {
    let popup_layout = center(Direction::Vertical, percent_y, r);
    center(Direction::Horizontal, percent_x, popup_layout)
}