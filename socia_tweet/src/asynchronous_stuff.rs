use std::borrow::Borrow;
use std::collections::{BTreeSet, HashMap};
use std::sync::Arc;

use algorithms::alg_data::{EstimatedClass, TweetData, WordClassifier};
use algorithms::bayes::{Bigram, BiUnigram, Unigram};
use algorithms::keyword::{classify_keyword, create_keyword_classifiers};
use algorithms::knn::classify;
use algorithms::n_grams::Ngram;
use csv::{ReaderBuilder, StringRecord};
use lazy_static::lazy_static;
use regex::Regex;
use tokio::sync::Mutex;
use twitter_v2::{Error, Tweet};
use twitter_v2::authorization::BearerToken;

use crate::App;
use crate::data::TweetDataDisplay;
use crate::tweet_fetcher::fetch;

pub enum TasksEvent {
    FetchTweeter,
    ClassifyWithKeywords,
    ClassifyWithKNN,
    ClassifyWithUnigram,
    ClassifyWithBigram,
    ClassifyWithBiUnigram,
}

#[derive(Clone)]
pub struct TaskHandler<'a> {
    tweeter_auth_token: String,
    positive_keywords: BTreeSet<String>,
    negative_keywords: BTreeSet<String>,
    knn_classifier: Vec<TweetData>,
    unigram_classifier: HashMap<String, WordClassifier>,
    bigram_classifier: HashMap<String, WordClassifier>,
    bi_unigram_classifier: HashMap<String, WordClassifier>,
    regexes:Vec<(Regex, String)>,
    pub app: &'a Arc<Mutex<App>>,
}

impl<'a> TaskHandler<'a> {
    pub fn new(app: &'a Arc<Mutex<App>>) -> Self {
        let token = String::from("AAAAAAAAAAAAAAAAAAAAADf9gwEAAAAAtUMkjIu70A2Gg%2FJGkeZN87kVRRM%3DNtcCMhavc9k9EzD8USFXFCVjqYbqS6bZdLA6gS8GQXvfLsaNWe");
        let (pos, neg, knn, uni, bi, bi_uni) = create_classifiers();

        TaskHandler {
            tweeter_auth_token: token,
            positive_keywords: pos,
            negative_keywords: neg,
            knn_classifier: knn,
            unigram_classifier: uni,
            bigram_classifier: bi,
            bi_unigram_classifier: bi_uni,
            regexes: create_regexes(),
            app,
        }
    }

    pub async fn dispatch_task(&mut self, task: TasksEvent) {
        match task {
            TasksEvent::FetchTweeter => { self.fetch_tweets().await }
            TasksEvent::ClassifyWithKeywords => { self.classify_with_keywords().await }
            TasksEvent::ClassifyWithKNN => { self.classify_with_knn().await }
            TasksEvent::ClassifyWithUnigram => { self.classify_with_unigram().await }
            TasksEvent::ClassifyWithBigram => { self.classify_with_bigram().await }
            TasksEvent::ClassifyWithBiUnigram => { self.classify_with_bi_unigram().await }
        };
        let mut app = self.app.lock().await;
        app.show_popup = false;
    }

    async fn fetch_tweets(&mut self) {
        let token = BearerToken::new(&self.tweeter_auth_token);
        let mut app = self.app.lock().await;
        let keyword: String = String::from(app.input.drain(..).collect::<String>());
        let fetch_res = fetch(token, &keyword, None).await;

        match fetch_res {
            Some(tweets) => { app.tweets = convert_tweets_to_display(tweets) }
            None => {}
        };
    }

    async fn classify_with_keywords(&mut self) {
        let mut app = self.app.lock().await;
        let old_tweets = &app.tweets;

        let pos = &self.positive_keywords;
        let neg = &self.negative_keywords;
        let new_tweets = old_tweets.iter().map(|t| {
            let text = transform_tweet(&self.regexes, &t.content);
            let c = classify_keyword(&text, pos, neg);
            TweetDataDisplay {
                name: String::from(&t.name),
                content: String::from(&t.content),
                tweet_id: String::from(&t.tweet_id),
                keyword_estimation: Some(c),
                knn_estimation: t.knn_estimation,
                unigram_estimation: t.unigram_estimation,
                bigram_estimation: t.bigram_estimation,
                bi_unigram_estimation: t.bi_unigram_estimation,
            }
        }).collect();

        app.tweets = new_tweets;
    }

    async fn classify_with_knn(&mut self) {
        let mut app = self.app.lock().await;
        let old_tweets = &app.tweets;

        let classifier = &self.knn_classifier;
        let new_tweets = old_tweets.iter().map(|t| {
            let text = transform_tweet(&self.regexes, &t.content);
            let c = classify(&text, classifier);
            TweetDataDisplay {
                name: String::from(&t.name),
                content: String::from(&t.content),
                tweet_id: String::from(&t.tweet_id),
                keyword_estimation: t.keyword_estimation,
                knn_estimation: Some(c),
                unigram_estimation: t.unigram_estimation,
                bigram_estimation: t.bigram_estimation,
                bi_unigram_estimation: t.bi_unigram_estimation,
            }
        }).collect();

        app.tweets = new_tweets;
    }

    async fn classify_with_unigram(&mut self) {
        let mut app = self.app.lock().await;
        let old_tweets = &app.tweets;

        let classifier = &self.unigram_classifier;
        let new_tweets = old_tweets.iter().map(|t| {
            let text = transform_tweet(&self.regexes, &t.content);
            let c = Unigram::classify_tweet(&text, classifier);
            TweetDataDisplay {
                name: String::from(&t.name),
                content: String::from(&t.content),
                tweet_id: String::from(&t.tweet_id),
                keyword_estimation: t.keyword_estimation,
                knn_estimation: t.knn_estimation,
                unigram_estimation: Some(c),
                bigram_estimation: t.bigram_estimation,
                bi_unigram_estimation: t.bi_unigram_estimation,
            }
        }).collect();

        app.tweets = new_tweets;
    }

    async fn classify_with_bigram(&mut self) {
        let mut app = self.app.lock().await;
        let old_tweets = &app.tweets;

        let classifier = &self.bigram_classifier;
        let new_tweets = old_tweets.iter().map(|t| {
            let text = transform_tweet(&self.regexes, &t.content);
            let c = Bigram::classify_tweet(&text, classifier);
            TweetDataDisplay {
                name: String::from(&t.name),
                content: String::from(&t.content),
                tweet_id: String::from(&t.tweet_id),
                keyword_estimation: t.keyword_estimation,
                knn_estimation: t.knn_estimation,
                unigram_estimation: t.unigram_estimation,
                bigram_estimation: Some(c),
                bi_unigram_estimation: t.bi_unigram_estimation,
            }
        }).collect();

        app.tweets = new_tweets;
    }

    async fn classify_with_bi_unigram(&mut self) {
        let mut app = self.app.lock().await;
        let old_tweets = &app.tweets;

        let classifier = &self.bi_unigram_classifier;
        let new_tweets = old_tweets.iter().map(|t| {
            let text = transform_tweet(&self.regexes, &t.content);
            let c = BiUnigram::classify_tweet(&text, classifier);
            TweetDataDisplay {
                name: String::from(&t.name),
                content: String::from(&t.content),
                tweet_id: String::from(&t.tweet_id),
                keyword_estimation: t.keyword_estimation,
                knn_estimation: t.knn_estimation,
                unigram_estimation: t.unigram_estimation,
                bigram_estimation: t.bigram_estimation,
                bi_unigram_estimation: Some(c),
            }
        }).collect();

        app.tweets = new_tweets;
    }
}

fn create_regexes() -> Vec<(Regex, String)> {
    vec![
        (Regex::new(r"@[A-Za-z\d]*").unwrap(), String::from("@")),
        (Regex::new(r"#[A-Za-z\d]*").unwrap(), String::from("{htag}")),
        (Regex::new(r"https://t.co/[A-Za-z\d]*").unwrap(), String::from("{tCo}")),
        (Regex::new(r"https://[A-Za-z\d/.?]*").unwrap(), String::from("{link}")),
        (Regex::new(r"http://[A-Za-z\d/.?]*").unwrap(), String::from("{link}")),
        (Regex::new(r"\$\d+").unwrap(), String::from("$XXX")),
        (Regex::new(r"\d+%").unwrap(), String::from("XXX€")),
        (Regex::new(r"\d*€").unwrap(), String::from("XX%")),
    ]
}

fn transform_tweet(regexes:&Vec<(Regex, String)>, tweet: &str) -> String {
    let mut s: String = tweet.to_string();

    for r in regexes {
        let (regex, replacement) = r;
        s = regex.replace_all(s.borrow(), replacement).to_string();
    }
    s
}

fn convert_tweets_to_display(tweets: Vec<Tweet>) -> Vec<TweetDataDisplay> {
    let mut v = Vec::new();
    for t in tweets {
        let tweet = TweetDataDisplay::new(String::from(""), String::from(t.text), String::from(t.id.to_string()));
        v.push(tweet);
    }
    v
}

fn create_classifiers() -> (
    BTreeSet<String>,
    BTreeSet<String>,
    Vec<TweetData>,
    HashMap<String, WordClassifier>,
    HashMap<String, WordClassifier>,
    HashMap<String, WordClassifier>
) {
    let kw = create_keyword_classifiers();
    let (pos, neg) = kw.unwrap();
    let knn = load_classified_tweets();
    let unigram = Unigram::create_classifier(&knn);
    let bigram = Bigram::create_classifier(&knn);
    let bi_unigram = BiUnigram::create_classifier(&knn);
    (pos, neg, knn, unigram, bigram, bi_unigram)
}

fn estimated_class_from_u8(c: u8) -> Option<EstimatedClass> {
    match c {
        2 => Some(EstimatedClass::Positive),
        1 => Some(EstimatedClass::Neutral),
        0 => Some(EstimatedClass::Negative),
        _ => None
    }
}

fn tweet_data_from_string_record(sr: StringRecord) -> TweetData {
    let mut iter = &mut sr.iter();
    iter.next();
    iter.next();
    iter.next();
    let content = String::from(iter.next().unwrap());
    iter.next();
    let c = iter.next().unwrap();
    let classification = c.parse::<u8>();
    match classification {
        Ok(class) => TweetData::new(estimated_class_from_u8(class), content),
        Err(e) => panic!("{c}: {e}")
    }
}

pub fn load_classified_tweets() -> Vec<TweetData> {
    let mut reader = ReaderBuilder::new().delimiter(b'|')
        .from_path("res/random_tweets.csv")
        .expect("Houston, no file found !");

    let mut tweets = Vec::new();
    for record in reader.records() {
        let tweet = tweet_data_from_string_record(record.expect("Error on the current record...."));
        tweets.push(tweet);
    }
    tweets
}

