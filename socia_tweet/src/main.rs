mod tweet_fetcher;
mod ui;
mod data;
mod asynchronous_stuff;

use std::{error::Error, io};
use std::sync::Arc;
use std::sync::mpsc::Receiver;
use tokio::sync::Mutex;
use crossterm::{
    event::{self, DisableMouseCapture, EnableMouseCapture, Event, KeyCode},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
// use tokio::sync::Mutex;
use tui::{
    backend::{Backend, CrosstermBackend},
    Terminal,
};
use ui::{App, draw_main_layout, InputMode};
use crate::asynchronous_stuff::{TaskHandler, TasksEvent};

/// App holds the state of the application
#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let (event_sender, event_receiver) = std::sync::mpsc::channel::<TasksEvent>();

    // create app
    let app = Arc::new(Mutex::new(App::new(event_sender)));
    // clone it for the other thread
    let cloned_app = Arc::clone(&app);
    //     ^ this goes there v
    std::thread::spawn(move || {
        let mut task_handler = TaskHandler::new(&cloned_app);
        run_data_thread(&mut task_handler, event_receiver);
    });

    let res = run_ui_thread(app).await;

    if let Err(err) = res {
        println!("{:?}", err)
    }

    Ok(())
}

#[tokio::main]
async fn run_data_thread(task_handler:&mut TaskHandler, event_receiver:Receiver<TasksEvent>) {
    while let Ok(task) = event_receiver.recv() {
        task_handler.dispatch_task(task).await;
    }
}

async fn run_ui_thread(app:Arc<Mutex<App>>) -> Result<(), Box<dyn Error>> {
    // setup terminal
    enable_raw_mode()?;
    let mut stdout = io::stdout();
    execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    let res = run_app(&mut terminal, app).await;

    // restore terminal
    disable_raw_mode()?;
    execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    )?;
    terminal.show_cursor()?;
    Ok(())
}

async fn run_app<B: Backend>(terminal: &mut Terminal<B>, app: Arc<Mutex<App>>) -> io::Result<()> {
    loop {
        let mut app = app.lock().await;

        terminal.draw(|f| draw_main_layout(f, &app))?;

        if let Event::Key(key) = event::read()? {
            match app.show_popup {
                true => {
                    match app.input_mode {
                        InputMode::Normal => if key.code == KeyCode::Char('q') { return Ok(()); },
                        _ => {}
                    }
                }
                false => {
                    match app.input_mode {
                        InputMode::Normal => match key.code {
                            // Search a tweet
                            KeyCode::Char('s') => {
                                app.input_mode = InputMode::Editing;
                            }
                            // quit the app
                            KeyCode::Char('q') => {
                                return Ok(());
                            }
                            // perform a dictionnary classification
                            KeyCode::Char('d') => {
                                app.dispatch(TasksEvent::ClassifyWithKeywords);
                            }
                            // perform a knn classification
                            KeyCode::Char('k') => {
                                app.dispatch(TasksEvent::ClassifyWithKNN);
                            }
                            // perform a unigram-based bayes classification
                            KeyCode::Char('v') => {
                                app.dispatch(TasksEvent::ClassifyWithUnigram);
                            }
                            // perform a bigram-based bayes classification
                            KeyCode::Char('b') => {
                                app.dispatch(TasksEvent::ClassifyWithBigram);
                            }
                            // perform a unigram+bigram-based bayes classification
                            KeyCode::Char('m') => {
                                app.dispatch(TasksEvent::ClassifyWithBiUnigram);
                            }
                            _ => {}
                        },
                        InputMode::Editing => match key.code {
                            KeyCode::Enter => {
                                app.input_mode = InputMode::Normal;
                                app.dispatch(TasksEvent::FetchTweeter)
                                // Send request to twitter WS
                            }
                            KeyCode::Char(c) => {
                                app.input.push(c);
                            }
                            KeyCode::Backspace => {
                                app.input.pop();
                            }
                            _ => {}
                        },
                    }
                }
            }
        }
    }
}


//    let messages: Vec<ListItem> = app
//    .messages
//    .iter()
//    .enumerate()
//    .map(|(i, m)| {
//        let content = vec![Spans::from(Span::raw(format!("{}: {}", i, m)))];
//        ListItem::new(content)
//    })
//    .collect();
//    let messages =
//    List::new(messages).block(Block::default().borders(Borders::ALL).title("Messages"));
//    f.render_widget(messages, chunks[2]);
//}
