pub mod bayes;
pub mod knn;
pub mod n_grams;

pub mod keyword;

pub mod alg_data {
    #[derive(Debug, Clone, Eq, PartialEq)]
    pub struct TweetData {
        pub class: Option<EstimatedClass>,
        pub content: String,
    }

    impl TweetData {
        pub fn new(class: Option<EstimatedClass>, content: String) -> Self {
            Self { class, content }
        }
    }

    #[derive(Debug, Copy, Clone, Eq, PartialEq)]
    pub enum EstimatedClass {
        Positive,
        Neutral,
        Negative,
    }

    #[derive(Debug, Copy, Clone, PartialEq)]
    pub struct WordClassifier {
        positive_coef: f64,
        negative_coef: f64,
        neutral_coef: f64,
    }

    impl Default for WordClassifier {
        fn default() -> Self {
            Self {
                positive_coef: 1.0,
                negative_coef: 1.0,
                neutral_coef: 1.0,
            }
        }
    }

    //    impl Default for &mut WordClassifier {
    //        fn default() -> Self {
    //            Self {
    //                positive_coef: 1.0,
    //                negative_coef: 1.0,
    //                neutral_coef: 1.0,
    //            }
    //        }
    //    }

    impl WordClassifier {
        pub fn new(positive_coef: f64, negative_coef: f64, neutral_coef: f64) -> Self {
            Self {
                positive_coef,
                negative_coef,
                neutral_coef,
            }
        }
        pub fn update(&mut self, other: &WordClassifier) -> &mut Self {
            self.positive_coef *= other.positive_coef;
            self.negative_coef *= other.negative_coef;
            self.neutral_coef *= other.neutral_coef;

            self
        }

        pub fn get_estimated(&self) -> EstimatedClass {
            if self.positive_coef > self.negative_coef
                && (self.positive_coef > self.neutral_coef || self.neutral_coef == 1.0)
            {
                EstimatedClass::Positive
            } else if (self.negative_coef > self.neutral_coef || self.neutral_coef == 1.0)
                && self.negative_coef != self.positive_coef
            {
                EstimatedClass::Negative
            } else {
                EstimatedClass::Neutral
            }
        }

        pub fn increment_for(&mut self, class: EstimatedClass) -> &mut Self {
            match class {
                EstimatedClass::Positive => self.positive_coef += 1.0,
                EstimatedClass::Neutral => self.neutral_coef += 1.0,
                EstimatedClass::Negative => self.negative_coef += 1.0,
            }

            self
        }

        pub fn normalize(&mut self, nb_word: impl Into<f64>) -> &mut Self {
            let nb_word = nb_word.into();
            self.positive_coef /= nb_word;
            self.negative_coef /= nb_word;
            self.neutral_coef /= nb_word;

            self
        }
    }
}

#[macro_use]
extern crate lazy_static;

use std::collections::HashSet;

lazy_static! {
    static ref NOT_IMPORTANT_WORDS: HashSet<&'static str> = {
        let mut h = HashSet::new();
        h.insert("j");
        h.insert("j'");
        h.insert("je");
        h.insert("tu");
        h.insert("il");
        h.insert("elle");
        h.insert("on");
        h.insert("nous");
        h.insert("vous");
        h.insert("ils");
        h.insert("elles");
        h.insert("le");
        h.insert("la");
        h.insert("les");
        h.insert("de");
        h.insert("du");
        h.insert("des");
        h.insert("se");
        h.insert("au");
        h.insert("aux");
        h.insert("mon");
        h.insert("ma");
        h.insert("mes");
        h.insert("ton");
        h.insert("ta");
        h.insert("tes");
        h.insert("son");
        h.insert("sa");
        h.insert("ses");
        h.insert("leur");
        h.insert("leurs");
        h.insert("notre");
        h.insert("nos");
        h.insert("votre");
        h.insert("vos");
        h.insert("ce");
        h.insert("cette");
        h.insert("ces");
        h.insert("un");
        h.insert("une");
        h
    };
}
