use std::collections::{BTreeSet, HashMap};

use crate::alg_data::{EstimatedClass, TweetData, WordClassifier};

fn normalize_map(
    map: HashMap<String, WordClassifier>,
    words_number: usize,
) -> HashMap<String, WordClassifier> {
    let mut ret_map: HashMap<String, WordClassifier> = HashMap::new();
    let nb_words = words_number + map.len();
    for (s, mut classifier) in map {
        let c = classifier.normalize(nb_words as f64);
        ret_map.insert(s, *c);
    }
    ret_map
}

pub trait Ngram {
    fn create_classifier(tweets: &Vec<TweetData>) -> HashMap<String, WordClassifier> {
        let (to_normalise, number_of_words) = Self::classificate(tweets);
        normalize_map(to_normalise, number_of_words)
    }

    fn create_splits(tweet: &str) -> BTreeSet<String>;

    fn classificate(tweets: &Vec<TweetData>) -> (HashMap<String, WordClassifier>, usize) {
        let mut map: HashMap<String, WordClassifier> = HashMap::new();
        let mut words = 0;

        for tweet in tweets {
            if let Some(class) = tweet.class {
                let splitted = Self::create_splits(&tweet.content);
                for word in splitted {
                    words += 1;
                    map.entry(word).or_default().increment_for(class);
                }
            }
        }

        (map, words)
    }

    fn classify_tweet(tweet: &str, map: &HashMap<String, WordClassifier>) -> EstimatedClass {
        let mut curr_classification = WordClassifier::default();
        let splitted = Self::create_splits(tweet);
        for word in splitted {
            let word: &str = &word;
            if let &Some(class) = &map.get(word) {
                curr_classification.update(&class);
            }
        }

        curr_classification.get_estimated()
    }
}
