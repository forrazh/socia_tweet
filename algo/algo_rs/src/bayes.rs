// make a map of indexed words
use std::collections::{BTreeSet, HashMap, HashSet};

use crate::{
    alg_data::{EstimatedClass, TweetData, WordClassifier},
    n_grams::Ngram,
    NOT_IMPORTANT_WORDS,
};

fn filter_important(word: &str) -> bool {
    !NOT_IMPORTANT_WORDS.contains(word)
}

#[test]
fn test_filter_important() {
    let word_important = "socialiste";
    let word_not_important = "du";
    assert_eq!(true, filter_important(word_important));
    assert_eq!(false, filter_important(word_not_important));
}

pub struct Unigram;
impl Ngram for Unigram {
    fn create_splits(tweet: &str) -> BTreeSet<String> {
        let lowercase = tweet.to_ascii_lowercase();
        let splitted: BTreeSet<String> = lowercase
            .split_whitespace()
            .filter(|word| filter_important(word))
            .map(|word| String::from(word))
            .collect();
        splitted
    }
}

pub struct Bigram;
impl Ngram for Bigram {
    fn create_splits(tweet: &str) -> BTreeSet<String> {
        let lowercase = tweet.to_ascii_lowercase();
        let splitted: Vec<&str> = lowercase
            .split_whitespace()
            .filter(|word| filter_important(word))
            .collect();
        let mut ret: BTreeSet<String> = BTreeSet::new();
        if !splitted.is_empty() {
            for i in 0..(splitted.len() - 1) {
                let w1 = String::from(splitted[i]);
                let w2 = splitted[i + 1];
                let w = w1 + " " + w2;
                ret.insert(w);
            }
        }

        ret
    }
}

pub struct BiUnigram;
impl Ngram for BiUnigram {
    fn create_splits(tweet: &str) -> BTreeSet<String> {
        let uni_set = Unigram::create_splits(tweet);
        let bi_set = Bigram::create_splits(tweet);
        &uni_set | &bi_set
    }
}

#[test]
fn test_unigram_classifier() {
    let others: Vec<TweetData> = vec![
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("Je déteste la TAXIDERMIE"), // 2
        },
        TweetData {
            class: Some(EstimatedClass::Neutral),
            content: String::from("Les avions volent haut dans le ciel."), // 5
        },
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("socialiste"), // 1
        },
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("Deux avions se sont percutés au TexaS ! Je n' aime pas ça !"), // 10
        },
        TweetData {
            class: Some(EstimatedClass::Positive),
            content: String::from("Youhou ! Marc aime mon TP <3"), // 6
        },
        TweetData {
            class: Some(EstimatedClass::Positive),
            content: String::from("J' aime les pates et j' aime les chiens"), // 5
        },
        TweetData {
            class: Some(EstimatedClass::Positive),
            content: String::from("J' aime les chiens"), // 2
        },
    ];

    let map = Unigram::create_classifier(&others);
    let size = (31 + 23) as f64;

    let youhou_expected = WordClassifier::new(2.0 / size, 1.0 / size, 1.0 / size);
    let youhou_actual = *map.get("youhou").unwrap();
    assert_eq!(youhou_expected, youhou_actual);

    let taxidermie_expected = WordClassifier::new(1.0 / size, 2.0 / size, 1.0 / size);
    let taxidermie_actual = *map.get("taxidermie").unwrap();
    assert_eq!(taxidermie_expected, taxidermie_actual);

    let avion_expected = WordClassifier::new(1.0 / size, 2.0 / size, 2.0 / size);
    let avion_actual = *map.get("avions").unwrap();
    assert_eq!(avion_expected, avion_actual);

    let marc_expected = WordClassifier::new(2.0 / size, 1.0 / size, 1.0 / size);
    let marc_actual = *map.get("marc").unwrap();
    assert_eq!(marc_expected, marc_actual);

    let aime_expected = WordClassifier::new(4.0 / size, 2.0 / size, 1.0 / size);
    let aime_actual = *map.get("aime").unwrap();
    assert_eq!(aime_expected, aime_actual);
}

#[test]
fn test_classify_unigram_nothing() {
    let expected = EstimatedClass::Neutral;

    let tweet = String::from("");
    let actual = Unigram::classify_tweet(&tweet, &HashMap::new());

    assert_eq!(expected, actual)
}

#[test]
fn test_bigram_classifier() {
    let others: Vec<TweetData> = vec![
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("Je déteste la TAXIDERMIE"), // 1
        },
        TweetData {
            class: Some(EstimatedClass::Neutral),
            content: String::from("Les avions volent haut dans le ciel."), //4
        },
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("socialiste"), // 0
        },
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("Deux avions se sont percutés au TexaS ! Je n' aime pas ça !"), // 10
        },
        TweetData {
            class: Some(EstimatedClass::Positive),
            content: String::from("Youhou ! Marc aime mon TP <3"), // 5
        },
        TweetData {
            class: Some(EstimatedClass::Positive),
            content: String::from("J' aime les pates et j' aime les chiens"), // 4
        },
        TweetData {
            class: Some(EstimatedClass::Positive),
            content: String::from("J' aime les chiens"), // 1
        },
    ];

    let map = Bigram::create_classifier(&others);
    let size = (24 + 25) as f64;

    let aime_chiens_expected = WordClassifier::new(3.0 / size, 1.0 / size, 1.0 / size);
    let aime_chiens_actual = *map.get("aime chiens").unwrap();
    assert_eq!(aime_chiens_expected, aime_chiens_actual);

    let deux_avions_expected = WordClassifier::new(1.0 / size, 2.0 / size, 1.0 / size);
    let deux_avions_actual = *map.get("deux avions").unwrap();
    assert_eq!(deux_avions_expected, deux_avions_actual);

    // too lazy to test the rest
}

#[test]
fn test_bi_unigram() {
    let others: Vec<TweetData> = vec![
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("Je déteste la TAXIDERMIE"), // 1
        },
        TweetData {
            class: Some(EstimatedClass::Neutral),
            content: String::from("Les avions volent haut dans le ciel."), //4
        },
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("socialiste"), // 0
        },
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("Deux avions se sont percutés au TexaS ! Je n' aime pas ça !"), // 10
        },
        TweetData {
            class: Some(EstimatedClass::Positive),
            content: String::from("Youhou ! Marc aime mon TP <3"), // 5
        },
        TweetData {
            class: Some(EstimatedClass::Positive),
            content: String::from("J' aime les pates et j' aime les chiens"), // 4
        },
        TweetData {
            class: Some(EstimatedClass::Positive),
            content: String::from("J' aime les chiens"), // 1
        },
    ];
    let map: HashMap<String, WordClassifier> = BiUnigram::create_classifier(&others);

    let size_uni: f64 = (31 + 23) as f64;
    let size_bi: f64 = (25 + 24) as f64;
    let size: f64 = size_uni + size_bi;

    let youhou_expected = WordClassifier::new(2.0 / size, 1.0 / size, 1.0 / size);
    let youhou_actual = *map.get("youhou").unwrap();
    assert_eq!(youhou_expected, youhou_actual);

    let aime_chiens_expected = WordClassifier::new(3.0 / size, 1.0 / size, 1.0 / size);
    let aime_chiens_actual = *map.get("aime chiens").unwrap();
    assert_eq!(aime_chiens_expected, aime_chiens_actual);
}

#[test]
fn test_classify_bigram_nothing() {
    let expected = EstimatedClass::Neutral;

    let tweet = String::from("");
    let actual = Bigram::classify_tweet(&tweet, &HashMap::new());

    assert_eq!(expected, actual)
}

#[test]
fn test_classify_bi_unigram_nothing() {
    let expected = EstimatedClass::Neutral;

    let tweet = String::from("");
    let actual = BiUnigram::classify_tweet(&tweet, &HashMap::new());

    assert_eq!(expected, actual)
}

#[test]
fn test_unigram_filled() {
    let size: f64 = 20.0;
    let classified_unigram: HashMap<String, WordClassifier> = HashMap::from([
        (
            String::from("aime"),
            WordClassifier::new(4.0 / size, 2.0 / size, 1.0 / size),
        ),
        (
            String::from("pates"),
            WordClassifier::new(2.0 / size, 2.0 / size, 3.0 / size),
        ),
        (
            String::from("déteste"),
            WordClassifier::new(1.0 / size, 3.0 / size, 1.0 / size),
        ),
        (
            String::from("pas"),
            WordClassifier::new(1.0 / size, 3.0 / size, 2.0 / size),
        ),
        (
            String::from("klurgh"),
            WordClassifier::new(2.0 / size, 4.0 / size, 3.0 / size),
        ),
        (
            String::from("oooh"),
            WordClassifier::new(2.0 / size, 1.0 / size, 2.0 / size),
        ),
        (
            String::from("ok"),
            WordClassifier::new(2.0 / size, 2.0 / size, 2.0 / size),
        ),
        (
            String::from("yeees"),
            WordClassifier::new(6.0 / size, 1.0 / size, 1.0 / size),
        ),
        (
            String::from("noooo"),
            WordClassifier::new(1.0 / size, 4.0 / size, 1.0 / size),
        ),
    ]);

    let to_test = vec![
        (
            EstimatedClass::Neutral,
            String::from("klurgh , des pates !"),
        ),
        (EstimatedClass::Positive, String::from("Yeees")),
        (EstimatedClass::Negative, String::from("Noooo")),
        (
            EstimatedClass::Negative,
            String::from("Je n' aime pas les pâtes et je déteste les chevaux"),
        ),
        (
            EstimatedClass::Positive,
            String::from("J' aime les dindons'"),
        ),
    ];

    for (expected, curr_tweet) in to_test {
        let actual = Unigram::classify_tweet(&curr_tweet, &classified_unigram);
        assert_eq!(expected, actual);
    }
}

#[test]
fn test_bigram_filled() {
    let size: f64 = 20.0;
    let classified_bigram: HashMap<String, WordClassifier> = HashMap::from([
        (
            String::from("n' aime"),
            WordClassifier::new(1.0 / size, 5.0 / size, 1.0 / size),
        ),
        (
            String::from("adore pates"),
            WordClassifier::new(2.0 / size, 1.0 / size, 1.0 / size),
        ),
        (
            String::from("déteste femme"),
            WordClassifier::new(1.0 / size, 3.0 / size, 1.0 / size),
        ),
        (
            String::from("pas temps"),
            WordClassifier::new(1.0 / size, 3.0 / size, 2.0 / size),
        ),
        (
            String::from("klurgh ,"),
            WordClassifier::new(2.0 / size, 4.0 / size, 3.0 / size),
        ),
    ]);

    let to_test = vec![
        (
            EstimatedClass::Negative,
            String::from("klurgh , des pates !"),
        ),
        (
            EstimatedClass::Negative,
            String::from("Je déteste cette femme !"),
        ),
        (
            EstimatedClass::Negative,
            String::from("Je déteste ces femme !"),
        ), // should output the same as above
        (
            EstimatedClass::Negative,
            String::from("Je déteste ta femme !"),
        ), // should output the same as above
        (
            EstimatedClass::Negative,
            String::from("Je déteste ma femme !"),
        ), // should output the same as above
        (
            EstimatedClass::Negative,
            String::from("Je n' aime pas les pâtes et je déteste les chevaux"),
        ),
        (
            EstimatedClass::Positive,
            String::from("J' adore les pates"),
        ),
        (
            EstimatedClass::Neutral,
            String::from("J' aime les dindons'"),
        ),
    ];

    for (expected, curr_tweet) in to_test {
        let actual = Bigram::classify_tweet(&curr_tweet, &classified_bigram);
        assert_eq!(expected, actual);
    }
}

#[test]
fn test_bi_unigram_filled() {
    let size: f64 = 40.0;
    let classified_bi_unigram: HashMap<String, WordClassifier> = HashMap::from([
        (
            String::from("aime"),
            WordClassifier::new(4.0 / size, 2.0 / size, 1.0 / size),
        ),
        (
            String::from("pates"),
            WordClassifier::new(2.0 / size, 2.0 / size, 3.0 / size),
        ),
        (
            String::from("déteste"),
            WordClassifier::new(1.0 / size, 3.0 / size, 1.0 / size),
        ),
        (
            String::from("pas"),
            WordClassifier::new(1.0 / size, 3.0 / size, 2.0 / size),
        ),
        (
            String::from("klurgh"),
            WordClassifier::new(2.0 / size, 4.0 / size, 3.0 / size),
        ),
        (
            String::from("oooh"),
            WordClassifier::new(2.0 / size, 1.0 / size, 2.0 / size),
        ),
        (
            String::from("ok"),
            WordClassifier::new(2.0 / size, 2.0 / size, 2.0 / size),
        ),
        (
            String::from("yes"),
            WordClassifier::new(6.0 / size, 1.0 / size, 1.0 / size),
        ),
        (
            String::from("no"),
            WordClassifier::new(1.0 / size, 6.0 / size, 1.0 / size),
        ),
        (
            String::from("n' aime"),
            WordClassifier::new(1.0 / size, 5.0 / size, 1.0 / size),
        ),
        (
            String::from("adore pates"),
            WordClassifier::new(2.0 / size, 1.0 / size, 1.0 / size),
        ),
        (
            String::from("déteste femme"),
            WordClassifier::new(1.0 / size, 3.0 / size, 1.0 / size),
        ),
        (
            String::from("pas temps"),
            WordClassifier::new(1.0 / size, 3.0 / size, 2.0 / size),
        ),
        (
            String::from("klurgh ,"),
            WordClassifier::new(2.0 / size, 4.0 / size, 3.0 / size),
        ),
    ]);

    let to_test = vec![
        (
            EstimatedClass::Negative,
            String::from("klurgh , des pates !"),
        ),
        (
            EstimatedClass::Negative,
            String::from("Je déteste cette femme !"),
        ),
        (EstimatedClass::Positive, String::from("Yes")),
        (EstimatedClass::Neutral, String::from("Yes no")),
        (EstimatedClass::Negative, String::from("No")),
        (
            EstimatedClass::Positive,
            String::from("J' adore les pates"),
        ),
        (
            EstimatedClass::Positive,
            String::from("J' aime les dindons'"),
        ),
    ];

    for (expected, curr_tweet) in to_test {
        let actual = BiUnigram::classify_tweet(&curr_tweet, &classified_bi_unigram);
        println!("{curr_tweet:?} -> e:{expected:?}; a:{actual:?}");
        assert_eq!(expected, actual);
    }
}

#[test]
fn test_full_bi_unigram() {
    let others: Vec<TweetData> = vec![
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("Je déteste la TAXIDERMIE"), // 1
        },
        TweetData {
            class: Some(EstimatedClass::Neutral),
            content: String::from("Les avions volent haut dans le ciel."), //4
        },
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("socialiste"), // 0
        },
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("Deux avions se sont percutés au TexaS ! Je n' aime pas ça !"), // 10
        },
        TweetData {
            class: Some(EstimatedClass::Positive),
            content: String::from("Youhou ! Marc adore mon TP <3"), // 5
        },
        TweetData {
            class: Some(EstimatedClass::Positive),
            content: String::from("J' aime les pates et j' aime les chiens"), // 4
        },
        TweetData {
            class: Some(EstimatedClass::Positive),
            content: String::from("J' aime les chiens"), // 1
        },
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("Je déteste la vie"), // 1
        },
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("Je déteste ton arrogance"), // 1
        },
        TweetData {
            class: Some(EstimatedClass::Positive),
            content: String::from("Oh yes go on!"), // 1
        },
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("Oh no, oh no, oh no no no."), // 1
        },
    ];
    let map: HashMap<String, WordClassifier> = BiUnigram::create_classifier(&others);

    let to_test = vec![
        (
            EstimatedClass::Negative,
            String::from("Je déteste cette femme !"),
        ),
        (EstimatedClass::Positive, String::from("Yes")),
        (EstimatedClass::Neutral, String::from("Yes no")),
        (EstimatedClass::Negative, String::from("No")),
        (
            EstimatedClass::Positive,
            String::from("J' adore les pates"),
        ),
        (
            EstimatedClass::Positive,
            String::from("J' aime les dindons'"),
        ),
    ];

    for (expected, curr_tweet) in to_test {
        let actual = BiUnigram::classify_tweet(&curr_tweet, &map);
        assert_eq!(expected, actual);
    }
}

#[test]
fn test_full_unigram() {
    let others: Vec<TweetData> = vec![
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("Je déteste la TAXIDERMIE"), // 1
        },
        TweetData {
            class: Some(EstimatedClass::Neutral),
            content: String::from("Les avions volent haut dans le ciel."), //4
        },
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("socialiste"), // 0
        },
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("Deux avions se sont percutés au TexaS ! Je n' aime pas ça !"), // 10
        },
        TweetData {
            class: Some(EstimatedClass::Positive),
            content: String::from("Youhou ! Marc adore mon TP <3"), // 5
        },
        TweetData {
            class: Some(EstimatedClass::Positive),
            content: String::from("J' aime les pates et j' aime les chiens"), // 4
        },
        TweetData {
            class: Some(EstimatedClass::Positive),
            content: String::from("J' aime les chiens"), // 1
        },
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("Je déteste la vie"), // 1
        },
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("Je déteste ton arrogance"), // 1
        },
        TweetData {
            class: Some(EstimatedClass::Positive),
            content: String::from("Oh yes go on!"), // 1
        },
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("Oh no, oh no, oh no no no."), // 1
        },
    ];
    let map: HashMap<String, WordClassifier> = Unigram::create_classifier(&others);

    let to_test = vec![
        (
            EstimatedClass::Negative,
            String::from("Je déteste cette femme !"),
        ),
        (EstimatedClass::Positive, String::from("Yes")),
        (EstimatedClass::Neutral, String::from("Yes no")),
        (EstimatedClass::Negative, String::from("No")),
        (
            EstimatedClass::Positive,
            String::from("J' adore les pates"),
        ),
        (
            EstimatedClass::Positive,
            String::from("J' aime les dindons'"),
        ),
    ];

    for (expected, curr_tweet) in to_test {
        let actual = Unigram::classify_tweet(&curr_tweet, &map);
        assert_eq!(expected, actual);
    }
}

#[test]
fn test_full_bigram() {
    let others: Vec<TweetData> = vec![
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("Je déteste la TAXIDERMIE"), // 1
        },
        TweetData {
            class: Some(EstimatedClass::Neutral),
            content: String::from("Les avions volent haut dans le ciel."), //4
        },
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("socialiste"), // 0
        },
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("Deux avions se sont percutés au TexaS ! Je n' aime pas ça !"), // 10
        },
        TweetData {
            class: Some(EstimatedClass::Positive),
            content: String::from("Youhou ! Marc adore mon TP <3"), // 5
        },
        TweetData {
            class: Some(EstimatedClass::Positive),
            content: String::from("J' aime les pates et j' aime les chiens"), // 4
        },
        TweetData {
            class: Some(EstimatedClass::Positive),
            content: String::from("J' aime les chiens"), // 1
        },
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("Je déteste la vie"), // 1
        },
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("Je déteste ton arrogance"), // 1
        },
        TweetData {
            class: Some(EstimatedClass::Positive),
            content: String::from("Oh yes go on!"), // 1
        },
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("Oh no, oh no, oh no no no."), // 1
        },
    ];
    let map: HashMap<String, WordClassifier> = Bigram::create_classifier(&others);

    let to_test = vec![
        (
            EstimatedClass::Neutral,
            String::from("Je déteste cette femme !"), //underfitting "deteste femme" n'a jamais ete observe
        ),
        (EstimatedClass::Negative, String::from("No no no")),
        (
            EstimatedClass::Neutral,
            String::from("J' adore les pates"), //underfitting "adore pates" n'a jamais ete observe
        ),
        (
            EstimatedClass::Positive,
            String::from("oh yes j' adore ta soeur hmm"),
        ),
    ];

    for (expected, curr_tweet) in to_test {
        let actual = Bigram::classify_tweet(&curr_tweet, &map);
        println!("{curr_tweet:?}");
        assert_eq!(expected, actual);
    }
}
