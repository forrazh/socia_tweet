use crate::alg_data::{EstimatedClass, TweetData};
use std::cmp;

#[test]
fn test_levenshtein_1() {
    let s1 = "J'adore les chiens";
    let s2 = "Je déteste les chiens";

    let result = get_levenshtein_distance(s1, s2);
    let expected = 7;
    assert_eq!(expected, result);
}

#[test]
fn test_levenshtein_2() {
    let s1 = r#"L’opération Spring est une opération militaire menée par les forces alliées, principalement le Canada, pendant la Seconde Guerre mondiale. Elle s'est déroulée au sud de Caen lors de la bataille de Normandie entre le 25 et le 27 juillet 1944. Elle a pour but de fixer les forces allemandes, principalement les divisions blindées, à l'est du front pour faciliter à l'ouest l'opération Cobra menée par les Américains qui tentent de percer le front dans le sud du Cotentin. Cette opération est menée par le 2e Corps canadien commandé par le lieutenant general (en France, général de corps d'armée) Guy Simonds. Elle s'oppose au gros des forces blindées allemandes, principalement le 1er Corps de SS-Panzer du SS-oberstgruppenführer (général de groupe d'armée SS) Sepp Dietrich qui obtient un succès défensif certain. "#;
    let s2 = r#"Dans la nuit du 24 au 25 juillet, à la lueur de projecteurs anti-aériens et avec l'appui de chars et de l'artillerie, Simonds lance l'infanterie canadienne au sud de Caen, sur les trois axes de May-sur-Orne, Verrières et Tilly-la-Campagne pour atteindre en profondeur Fontenay-le-Marmion, Rocquancourt et Garcelles-Secqueville, et peut-être ouvrir la route de Falaise. À l'exception de la prise du village de Verrières par le Royal Hamilton Light Infantry du lieutenant-colonel J. M. Rockingham, toutes les autres actions canadiennes échouent face à la résistance allemande de la nuit et de la matinée. Quand le commandement allié envisage de relancer de nouvelles actions en fin d'après-midi, ce sont les blindés allemands qui passent à la contre-attaque et repoussent les Canadiens sur leur ligne de départ. Cette opération est très coûteuse en vies humaines. Au total, elle cause plus de 1 500 pertes canadiennes, dont environ 450 tués au combat. C’est, pour les forces canadiennes, l’opération la plus importante en pertes humaines de la Seconde Guerre mondiale, après le débarquement de Dieppe, qui fait, sur environ 5 000 combattants, 3 367 pertes dont 907 morts au combat. "#;

    let result = get_levenshtein_distance(s1, s2);
    let expected = 849;
    assert_eq!(expected, result);
}

#[test]
fn test_levenshtein_3() {
    let s1 = "aabcc";
    let s2 = "bccdd";

    let result = get_levenshtein_distance(s1, s2);
    let expected = 4;
    assert_eq!(expected, result);
}

#[test]
fn test_levenshtein_4() {
    let s1 = "aaa";
    let s2 = "aaa";

    let result = get_levenshtein_distance(s1, s2);
    let expected = 0;
    assert_eq!(expected, result);
}

/// Function to get a distance between two tweets
///
/// Source: https://en.m.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#Rust
fn get_levenshtein_distance(s1: &str, s2: &str) -> usize {
    let v1: Vec<char> = s1.to_string().chars().collect();
    let v2: Vec<char> = s2.to_string().chars().collect();
    let v1len = v1.len();
    let v2len = v2.len();

    // Early exit if one of the strings is empty
    if v1len == 0 {
        return v2len;
    }
    if v2len == 0 {
        return v1len;
    }

    fn min3<T: Ord>(v1: T, v2: T, v3: T) -> T {
        cmp::min(v1, cmp::min(v2, v3))
    }
    fn delta(x: char, y: char) -> usize {
        if x == y {
            0
        } else {
            1
        }
    }

    let mut column: Vec<usize> = (0..v1len + 1).collect();
    for x in 1..v2len + 1 {
        column[0] = x;
        let mut lastdiag = x - 1;
        for y in 1..v1len + 1 {
            let olddiag = column[y];
            column[y] = min3(
                column[y] + 1,
                column[y - 1] + 1,
                lastdiag + delta(v1[y - 1], v2[x - 1]),
            );
            lastdiag = olddiag;
        }
    }
    column[v1len]
}

#[test]
fn test_classify_1() {
    let expected = EstimatedClass::Positive;
    let tweet = String::from("uhi");

    let others: Vec<TweetData> = vec![
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("non."),
        },
        TweetData {
            class: Some(EstimatedClass::Positive),
            content: String::from("ui"),
        },
        TweetData {
            class: Some(EstimatedClass::Neutral),
            content: String::from("boah"),
        },
        TweetData {
            class: Some(EstimatedClass::Positive),
            content: String::from("uhiiii"),
        },
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("socialiste"),
        },
        TweetData {
            class: Some(EstimatedClass::Neutral),
            content: String::from(""),
        },
        TweetData {
            class: Some(EstimatedClass::Positive),
            content: String::from("uuhi"),
        },
    ];

    let result = classify(&tweet, &others);
    assert_eq!(expected, result);
}
#[test]
fn test_classify_2() {
    let expected = EstimatedClass::Positive;
    let tweet = String::from("uhi");

    let others: Vec<TweetData> = vec![
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("non."),
        },
        TweetData {
            class: Some(EstimatedClass::Neutral),
            content: String::from("boah"),
        },
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("socialiste"),
        },
        TweetData {
            class: Some(EstimatedClass::Neutral),
            content: String::from(""),
        },
        TweetData {
            class: Some(EstimatedClass::Positive),
            content: String::from("uhi"),
        }, // same) value
    ];

    let result = classify(&tweet, &others);
    assert_eq!(expected, result);
}
#[test]
fn test_classify_3() {
    let expected = EstimatedClass::Neutral;
    let tweet =String::from("ddd");

    let others: Vec<TweetData> = vec![
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("aaa"),
        },
        TweetData {
            class: Some(EstimatedClass::Positive),
            content: String::from("bbb"),
        },
        TweetData {
            class: Some(EstimatedClass::Neutral),
            content: String::from("ccc"),
        },
        TweetData {
            class: Some(EstimatedClass::Negative),
            content: String::from("eee"),
        },
        TweetData {
            class: Some(EstimatedClass::Positive),
            content: String::from("fff"),
        },
        TweetData {
            class: Some(EstimatedClass::Neutral),
            content: String::from("ggg"),
        },
    ];

    let result = classify(&tweet, &others);
    assert_eq!(expected, result);
}
#[test]
fn test_classify_4() {
    // no value
    let expected = EstimatedClass::Neutral;
    let tweet = String::from("ddd");

    let others: Vec<TweetData> = vec![];

    let result = classify(&tweet, &others);
    assert_eq!(expected, result);
}

#[test]
fn test_classify_5() {
    let expected = EstimatedClass::Neutral;
    let tweet = String::from("ddd");

    let others: Vec<TweetData> = vec![
        TweetData {
            class: None,
            content: String::from("aaa"),
        },
        TweetData {
            class: None,
            content: String::from("bbb"),
        },
    ];

    let result = classify(&tweet, &others);
    assert_eq!(expected, result);
}

/// Function used to classify a tweet between its neighbors
pub fn classify(tweet: &str, neighbors: &Vec<TweetData>) -> EstimatedClass {
    if neighbors.is_empty() {
        return EstimatedClass::Neutral;
    }

    // Array containing couples of `usize` (distance from the current tweet) and `TweetData`
    let mut close_neighbors: Vec<(usize, &TweetData)> = vec![];

    for neighbor in neighbors {
        let dist = get_levenshtein_distance(tweet, &neighbor.content);
        close_neighbors.push((dist, neighbor));
    }

    close_neighbors.sort_by(|(dist_a, _), (dist_b, _)| dist_a.cmp(dist_b));

    if close_neighbors[0].0 == 0 {
        if let Some(val) = close_neighbors[0].1.class {
            return val;
        }
    }

    let closest = &close_neighbors[0..close_neighbors.len() / 2];

    let mut pos = 0;
    let mut neu = 0;
    let mut neg = 0;

    for item in closest {
        if let Some(class) = &(item.1.class) {
            match class {
                EstimatedClass::Positive => pos += 1,
                EstimatedClass::Neutral => neu += 1,
                EstimatedClass::Negative => neg += 1,
            }
        }
    }

    if pos > neg && pos > neu {
        return EstimatedClass::Positive;
    } else if neg > neu {
        return EstimatedClass::Negative;
    }

    EstimatedClass::Neutral
}
