use std::collections::{BTreeSet};

use std::io::{self, Read};

use std::fs::{File};
use std::path::Path;

use crate::{
    alg_data::{EstimatedClass, TweetData},
};

fn create_classifier_from_path<P: AsRef<Path>>(path: P) -> io::Result<BTreeSet<String>> {
    let mut file_p = File::open(path)?;
    let mut buf_p = vec![];
    file_p.read_to_end(&mut buf_p)?;
    let contents_p = String::from_utf8_lossy(&buf_p);

    let tree: BTreeSet<String> = contents_p
        .split(",")
        .map(|word| String::from(word))
        .collect();

    Ok(tree)
}

pub fn create_keyword_classifiers() -> io::Result<(BTreeSet<String>,BTreeSet<String>)> {
    let pos_tree = create_classifier_from_path("res/positive.txt")?;
    let neg_tree = create_classifier_from_path("res/negative.txt")?;

    Ok((pos_tree, neg_tree))
}

pub fn classify_keyword(tweet: &str, pos_tree:&BTreeSet<String>, neg_tree:&BTreeSet<String>) -> EstimatedClass {
    let mut pos_cpt = 0;
    let mut neg_cpt = 0;

    for item in pos_tree {
        if tweet.contains(&format!("{item} ")) {
            pos_cpt+=1;
        }
    }

    for item in neg_tree {
        if tweet.contains(&format!("{item} ")) {
            neg_cpt+=1;
        }
    }

    if pos_cpt>neg_cpt {
        EstimatedClass::Positive
    } else if neg_cpt>pos_cpt {
        EstimatedClass::Negative
    } else {
        EstimatedClass::Neutral
    }

}

#[test]
fn classify_keyword_test(){
    let (pos_words, neg_words) = create_keyword_classifiers().unwrap();

    let socia = String::from("some socialist nonsense");

    let socia_ex = EstimatedClass::Negative;
    let socia_val = classify_keyword(&socia, &pos_words, &neg_words);
    assert_eq!(socia_ex,socia_val);

    let lapin = String::from("J' aime manger des lapins");
    
    let lapin_ex = EstimatedClass::Positive;
    let lapin_val = classify_keyword(&lapin, &pos_words, &neg_words);
    assert_eq!(lapin_ex,lapin_val);

    let turlute = String::from("A quand la prochaine turlute");
    let turlute_ex = EstimatedClass::Neutral;
    let turlute_val = classify_keyword(&turlute, &pos_words, &neg_words);
    assert_eq!(turlute_ex,turlute_val);

    let chainsaw = String::from("Nouvelle illustration de chainsaw  man");
    let chainsaw_ex = EstimatedClass::Neutral;
    let chainsaw_val = classify_keyword(&chainsaw, &pos_words, &neg_words);
    assert_eq!(chainsaw_ex,chainsaw_val);
}