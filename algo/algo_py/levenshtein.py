import numpy as np
'''
def levenshtein(s1, s2):
    if len(s1) < len(s2):
        return levenshtein(s2, s1)

    # len(s1) >= len(s2)
    if len(s2) == 0:
        return len(s1)

    previous_row = range(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[j + 1] + 1 # j+1 instead of j since previous_row and current_row are one character longer
            deletions = current_row[j] + 1       # than s2
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row
    
    return previous_row[-1]
'''
def levenshtein(source, target):
    if len(source) < len(target):
        return levenshtein(target, source)

    # So now we have len(source) >= len(target).
    if len(target) == 0:
        return len(source)

    # We call tuple() to force strings to be used as sequences
    # ('c', 'a', 't', 's') - numpy uses them as values by default.
    source = np.array(tuple(source))
    target = np.array(tuple(target))

    # We use a dynamic programming algorithm, but with the
    # added optimization that we only need the last two rows
    # of the matrix.
    previous_row = np.arange(target.size + 1)
    for s in source:
        # Insertion (target grows longer than source):
        current_row = previous_row + 1

        # Substitution or matching:
        # Target and source items are aligned, and either
        # are different (cost of 1), or are the same (cost of 0).
        current_row[1:] = np.minimum(
                current_row[1:],
                np.add(previous_row[:-1], target != s))

        # Deletion (target grows shorter than source):
        current_row[1:] = np.minimum(
                current_row[1:],
                current_row[0:-1] + 1)

        previous_row = current_row

    return previous_row[-1]

def lev_phrase(s1,s2):
    tab_src=s1.split(" ")
    tab_targ=s2.split(" ")
    return levenshtein(tab_src,tab_targ)

#print(levenshtein(["endives","au","jambon"],["bro","les","endives","c'est","degueu"]))
'''
print(levenshtein("L’opération Spring est une opération militaire menée par les forces alliées, principalement le Canada, pendant la Seconde Guerre mondiale. Elle s'est déroulée au sud de Caen lors de"+
                  "la bataille de Normandie entre le 25 et le 27 juillet 1944. Elle a pour but de fixer les forces allemandes, principalement les divisions blindées, à l'est du front pour faciliter à"+
                  "l'ouest l'opération Cobra menée par les Américains qui tentent de percer le front dans le sud du Cotentin. Cette opération est menée par le 2e Corps canadien commandé par le lieutenant"+
                  "general (en France, général de corps d'armée) Guy Simonds. Elle s'oppose au gros des forces blindées allemandes, principalement le 1er Corps de SS-Panzer du SS-oberstgruppenführer"+
                  "(général de groupe d'armée SS) Sepp Dietrich qui obtient un succès défensif certain. ",
                  "Dans la nuit du 24 au 25 juillet, à la lueur de projecteurs anti-aériens et avec l'appui de chars et de l'artillerie, Simonds lance l'infanterie canadienne au sud de Caen, sur les trois"+
                  "axes de May-sur-Orne, Verrières et Tilly-la-Campagne pour atteindre en profondeur Fontenay-le-Marmion, Rocquancourt et Garcelles-Secqueville, et peut-être ouvrir la route de Falaise. À "+
                  "l'exception de la prise du village de Verrières par le Royal Hamilton Light Infantry du lieutenant-colonel J. M. Rockingham, toutes les autres actions canadiennes échouent face à la"+
                  "résistance allemande de la nuit et de la matinée. Quand le commandement allié envisage de relancer de nouvelles actions en fin d\'après-midi, ce sont les blindés allemands qui passent"+
                  "à la contre-attaque et repoussent les Canadiens sur leur ligne de départ. Cette opération est très coûteuse en vies humaines. Au total, elle cause plus de 1 500 pertes canadiennes,"+
                  "dont environ 450 tués au combat. C’est, pour les forces canadiennes, l’opération la plus importante en pertes humaines de la Seconde Guerre mondiale, après le débarquement de Dieppe,"+
                  "qui fait, sur environ 5 000 combattants, 3 367 pertes dont 907 morts au combat. "))
'''
print(levenshtein("je déteste les chiens","j'adore les chiens"))